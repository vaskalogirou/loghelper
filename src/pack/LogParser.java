package pack;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class LogParser {
	public static void main(String[] args) throws IOException {
		Map<String, Integer> map = new TreeMap<String, Integer>();
		List<String> lines = Utilities.readLines("src/logs.txt");
		for (String line : lines) {
			line = line.trim();
			if (line.isEmpty()) {
				break;
			}
			String className = Utilities.getClassFromLine(line);
			if (className != null) {
				if (map.containsKey(className)) {
					Integer currentValue = map.get(className);
					map.put(className, currentValue + 1);
				} else {
					map.put(className, new Integer(1));
				}
			}
		}
		Printer.printMap(map);
		System.out.println("all done");
	}
}
