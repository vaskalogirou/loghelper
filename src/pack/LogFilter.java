package pack;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class LogFilter {
	public static void main(String[] args) throws IOException, ParseException {
		String name = "5";
		String file = "C:\\Users\\Vasilis\\OneDrive\\txts\\logs\\" + name;
		List<String> lines = Utilities.readLines(file + ".log");
		String result = "";
		for (String line : lines) {
			line = Utilities.beautify(line);
			if (StringUtils.containsIgnoreCase(line, "error") || StringUtils.containsIgnoreCase(line, "warning")
					|| StringUtils.containsIgnoreCase(line, "exception")) {
				result += line;
			}
			if (StringUtils.containsIgnoreCase(line, "app[heroku-postgres]")) {
				continue;
			}
			if (StringUtils.containsIgnoreCase(line, "heroku[router]")) {
				continue;
			}
			if (StringUtils.containsIgnoreCase(line, "app[postgres.10]")) {
				continue;
			}
			result += line + System.lineSeparator();
		}
		Utilities.write(file + "_filtered.log", result);
		System.out.println("all done");
	}
}
