package pack;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Printer {
	public static void print(List<String> lista) {
		for (String str : lista) {
			System.out.println(str);
		}
	}

	public static void printMap(Map<String, Integer> map) {
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			String key = entry.getKey();
			key = key.substring(13);
			System.out.println(key + " " + entry.getValue());
		}
	}

	public static void print(Set<String> set) {
		for (String str : set) {
			System.out.println(str);
		}
	}
}
