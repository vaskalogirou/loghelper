package pack;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Utilities {
	public static List<String> readLines(String path) throws IOException {
		List<String> result = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(path));
		try {
			String line = br.readLine();
			while (line != null) {
				line = line.trim();
				result.add(line);
				line = br.readLine();
			}
		} finally {
			br.close();
		}
		return result;
	}

	public static void write(String path, String text) throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter(path)) {
			out.println(text);
		}
	}

	public static String getClassFromLine(String line) {
		int start = line.indexOf("com.owiwi.ca");
		if (start == -1) {
			return null;
		}
		String result = line.substring(start);
		int end = result.indexOf("()");
		if (end == -1) {
			System.out.println(result);
			return null;
		}
		result = result.substring(0, end);
		return result;
	}

	public static String getEndpointFromLine(String line) {
		int start = line.indexOf("heroku[router]: at=info method=");
		if (start == -1) {
			return null;
		}
		String result = line.substring(start);
		int end = result.indexOf("host=captain-amerika-production");
		if (end == -1) {
			System.out.println(result);
			return null;
		}
		result = result.substring(31, end);
		String[] tokens = result.split("\\?");
		result = tokens[0];

		if (result.contains("path=")) {
			result = result.replace("path=", "");
		}

		if (result.contains("\"")) {
			result = result.replace("\"", "");
		}

		if (result.contains("  ")) {
			result = result.replace("  ", " ");
		}

		String[] moreTokens = result.split(" ");
		if (moreTokens.length == 2) {
			result = moreTokens[1].trim() + ":" + moreTokens[0].trim();
		}

		return result;
	}

	public static String getMethodFromLine(String line) {
		int start = line.indexOf("LoggingAspect");
		if (start == -1) {
			return null;
		}
		String result = line.substring(start + 15);
		//		int end = result.indexOf(" with ");
		//		if (end == -1) {
		//			System.out.println(result);
		//			return null;
		//		}
		//		result = result.substring(0, end);
		result = result.replace("com.owiwi.ca.service.", "service.");
		result = result.replace("com.owiwi.ca.web.rest.", "web.rest.");
		return result;
	}

	public static List<String> getFileNames(String path) {
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();

		List<String> fileNames = new ArrayList<String>();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				fileNames.add(listOfFiles[i].getName());
			} else if (listOfFiles[i].isDirectory()) {
				System.out.println("Directory " + listOfFiles[i].getName());
			}
		}
		return fileNames;
	}

	public static String beautify(String line) throws ParseException {
		if (line == null || line.trim().isEmpty()) {
			return null;
		}
		String[] tokens = line.split(" ");
		DateFormat fullInput = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		DateFormat fullOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

		String result = "";
		for (int i = 0; i < tokens.length; i++) {
			if (i == 0) {
				Date fullDate = fullInput.parse(tokens[0]);
				result += fullOutput.format(fullDate) + " ";
			} else if (i == 2) {
				try {
					dateFormat.parse(tokens[2]);
				} catch (Exception exc) {
					result += tokens[2] + " ";
				}
			} else if (i == 3) {
				try {
					timeFormat.parse(tokens[3]);
				} catch (Exception exc) {
					result += tokens[3] + " ";
				}
			} else {
				result += tokens[i] + " ";
			}
		}
		return result;
	}
}
