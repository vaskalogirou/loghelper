package pack;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class EndpointFinder {

	public static void main(String[] args) throws IOException {
		String logFolder = "C:\\Users\\Alki\\OneDrive\\txts\\logs\\";
		Set<String> result = new TreeSet<String>();
		List<String> fileNames = Utilities.getFileNames(logFolder);
		for (String str : fileNames) {
			if (str.contains("filtered")) {
				continue;
			}
			List<String> lines = Utilities.readLines(logFolder + str);
			Set<String> endpoints = endpoints(lines);
			result.addAll(endpoints);
		}
		Printer.print(result);
	}

	public static Set<String> endpoints(List<String> lines) {
		Set<String> set = new TreeSet<>();
		for (String str : lines) {
			if (!str.contains("heroku[router]:")) {
				continue;
			}
			String endpoint = Utilities.getEndpointFromLine(str);
			if (endpoint != null) {
				set.add(endpoint);
			}
		}
		return set;
	}
}
